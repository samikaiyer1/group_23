import React from 'react';
import './App.css';
import NavBar from './Pages/NavBar';

function App() {
  return (
    <>
      <NavBar expand='lg'/>
    </>
  );
}

export default App;