
import { GoogleMap, Marker, useLoadScript } from "@react-google-maps/api";
import { useMemo } from "react";
import '../App.css';

export default function Map(props) {
    const {latitude, longitude} = props;
    const { isLoaded } = useLoadScript({
        googleMapsApiKey: "AIzaSyCknBsWd_CB3NIB0jIL4RBFlR9Vs9ZNDP4",
    });

    const mapContainerStyle = {
        width: '80%', 
    };

    const center = useMemo(() => ({ lat: latitude, lng: longitude }), []);
    return (
        <div style={mapContainerStyle}>
        {!isLoaded ? (
            <h1>Loading...</h1>
        ) : (
            <GoogleMap 
            mapContainerClassName="map-container"
            center={center}
            zoom={17}
            options={{
                mapTypeId: 'satellite',
            }}

            mapContainerStyle={{
                width: '100%', // Adjust the width as needed
                height: '100vh', 
            }}
            >
                <Marker position={{ lat: latitude, lng: longitude }} />
            </GoogleMap>

        )}
        </div>
    );
}