import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // for expect assertions

import ZipcodesFilters from './ZipcodesFilters';
import NursingHomesFilters from './ZipcodesFilters';
import GeriatriciansFilters from './ZipcodesFilters';

describe('ZipcodesFilters Component', () => {
  test('renders without crashing', () => {
    const { container } = render(
      <ZipcodesFilters
        selectedCities={[]}
        setSelectedCities={() => {}}
        selectedCounties={[]}
        setSelectedCounties={() => {}}
        populationValues={[0, 10000]}
        setPopulationValues={() => {}}
        ageValues={[20, 60]}
        setAgeValues={() => {}}
        incomeValues={[20000, 80000]}
        setIncomeValues={() => {}}
        setPage={() => {}}
      />
    );
    expect(container).toBeInTheDocument();
  });
});

describe('NursingHomesFilters Component', () => {
    test('renders without crashing', () => {
      const { container } = render(
        <NursingHomesFilters
          selectedCities={[]}
          setSelectedCities={() => {}}
          selectedCouncils={[]}
          setSelectedCouncils={() => {}}
          bedsValues={[0, 100]}
          setBedsValues={() => {}}
          ratingValues={[1, 5]}
          setRatingValues={() => {}}
          healthValues={[1, 5]}
          setHealthValues={() => {}}
          setPage={() => {}}
        />
      );
      expect(container).toBeInTheDocument();
    });
  });

  describe('GeriatriciansFilters Component', () => {
    test('renders without crashing', () => {
      const { container } = render(
        <GeriatriciansFilters
          selectedNames={[]}
          setSelectedNames={() => {}}
          selectedCities={[]}
          setSelectedCities={() => {}}
          selectedGenders={[]}
          setSelectedGenders={() => {}}
          selectedSchools={[]}
          setSelectedSchools={() => {}}
          yearValues={[1950, 2022]}
          setYearValues={() => {}}
          setPage={() => {}}
        />
      );
      expect(container).toBeInTheDocument();
    });
  });
