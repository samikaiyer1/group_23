import React from 'react';
import { 
    RadialBarChart, 
    RadialBar, 
    Legend, 
    ResponsiveContainer 
} from 'recharts';

const style = {
  top: '50%',
  right: 0,
  transform: 'translate(0, -50%)',
  lineHeight: '24px',
  backgroundColor: '#193B8F',
  padding: '10px', 
};

const transformData = (data) => {
  return data.map((item) => ({
    name: item.city,
    count: item.count,
    fill: item.fill,
  }));
};

const RadialChart = ({ data }) => (
  <ResponsiveContainer width="100%" height={600}>
    <RadialBarChart cx="50%" cy="50%" innerRadius="10%" outerRadius="80%" barSize={10} data={transformData(data)}>
      <RadialBar
        minAngle={15}
        label={{
          position: 'insideStart',
          fill: '#000',
        }}
        background
        clockWise
        dataKey="count"
      />
      <Legend iconSize={10} layout="vertical" verticalAlign="middle" wrapperStyle={style} />
    </RadialBarChart>
  </ResponsiveContainer>
);

export default RadialChart;