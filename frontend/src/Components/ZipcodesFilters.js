import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownItem from './DropdownItem'
import RangeItem from './RangeItem'
import SelectDropdown from '../Components/SelectDropdown';

const cities = [
    "FRISCO",
    "CARROLLTON",
    "ALLEN",
    "PLANO",
    "HALTOM CITY",
    "GRAPEVINE",
    "DALLAS",
    "FORT WORTH",
    "RICHARDSON",
    "KELLER",
    "MCKINNEY"
  ];

  const counties = [
    "TARRANT",
    "COLLIN",
    "DENTON",
    "DALLAS"
  ];

export default function ZipcodesFilters(props){
    const {
        sortName,
        setSortName,
        setSort,
        sortAttribs,
        descName,
        setDescName,
        setDesc,
        descAttribs,
        selectedCities,
        setSelectedCities,
        selectedCounties,
        setSelectedCounties,
        populationValues,
        setPopulationValues,
        ageValues,
        setAgeValues,
        incomeValues,
        setIncomeValues,
        setPage
      } = props;
      
      return (
        <div style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
            <SelectDropdown name={sortName} setName={setSortName} setSelected={setSort} setPage={setPage} list={sortAttribs}/>
            <SelectDropdown name={descName} setName={setDescName} setSelected={setDesc} setPage={setPage} list={descAttribs} />
            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Cities
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {cities.map((city) => (
                        <DropdownItem value={city} filters={selectedCities} setFilters={setSelectedCities} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Counties
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {counties.map((county) => (
                        <DropdownItem value={county} filters={selectedCounties} setFilters={setSelectedCounties} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Population
                </Dropdown.Toggle>
                <Dropdown.Menu style={{padding: '10%'}}>
                    <RangeItem values={populationValues} setValues={setPopulationValues} setPage={setPage}/>
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Median Age
                </Dropdown.Toggle>
                <Dropdown.Menu style={{padding: '10%'}}>
                    <RangeItem values={ageValues} setValues={setAgeValues} setPage={setPage}/>
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Average Income per Household
                </Dropdown.Toggle>
                <Dropdown.Menu style={{padding: '10%'}}>
                    <RangeItem values={incomeValues} setValues={setIncomeValues} setPage={setPage}/>
                </Dropdown.Menu>
            </Dropdown>
        </div>
      );
}