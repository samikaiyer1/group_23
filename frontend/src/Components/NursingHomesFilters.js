import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownItem from './DropdownItem';
import RangeItem from './RangeItem';
import SelectDropdown from '../Components/SelectDropdown';

const cities = [
    "DALLAS",
    "FORT WORTH",
    "RICHARDSON",
    "KELLER",   
    "MCKINNEY",
    "FRISCO",
    "CARROLLTON",
    "ALLEN",
    "WATAUGA",
    "RICHLAND HILLS",
    "PLANO",
    "WHITE SETTLEMENT",
    "GRAPEVINE"
  ];

  const councils = [
    "Both",
    "Resident",
    "Neither"
  ];

export default function NursingHomesFilters(props){
    const {
        sortName,
        setSortName,
        setSort,
        sortAttribs,
        descName,
        setDescName,
        setDesc,
        descAttribs,
        selectedCities,
        setSelectedCities,
        selectedCouncils,
        setSelectedCouncils,
        bedsValues,
        setBedsValues,
        ratingValues,
        setRatingValues,
        healthValues,
        setHealthValues,
        setPage
      } = props;
      
      return (
        <div style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
            <SelectDropdown name={sortName} setName={setSortName} setSelected={setSort} setPage={setPage} list={sortAttribs}/>
            <SelectDropdown name={descName} setName={setDescName} setSelected={setDesc} setPage={setPage} list={descAttribs} />
            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Cities
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {cities.map((city) => (
                        <DropdownItem value={city} filters={selectedCities} setFilters={setSelectedCities} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Council Type
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {councils.map((council) => (
                        <DropdownItem value={council} filters={selectedCouncils} setFilters={setSelectedCouncils} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Number of Certified Beds
                </Dropdown.Toggle>
                <Dropdown.Menu style={{padding: '10%'}}>
                    <RangeItem values={bedsValues} setValues={setBedsValues} setPage={setPage}/>
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Rating
                </Dropdown.Toggle>
                <Dropdown.Menu style={{padding: '10%'}}>
                    <RangeItem values={ratingValues} setValues={setRatingValues} setPage={setPage}/>
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Health Inspection Rating
                </Dropdown.Toggle>
                <Dropdown.Menu style={{padding: '10%'}}>
                    <RangeItem values={healthValues} setValues={setHealthValues} setPage={setPage}/>
                </Dropdown.Menu>
            </Dropdown>
        </div>
      );
}