import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown';
export default function DropdownSelectItem(props){
    const {name, setName, setSelected, attrib, setPage } = props;
  
    return (
      <Dropdown.Item
        onClick={() => {
          setSelected(attrib);
          setName(name);
          setPage(1);
        }}
      >
        {name}
      </Dropdown.Item>
    );
  
        
  }