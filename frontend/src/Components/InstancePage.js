import React, { useEffect, useState } from 'react';
import { useParams, useLocation } from 'react-router-dom';
import { Image } from 'react-bootstrap';
import SmallCard from './SmallCard';
import Map from './Map';
import GetNursingHomesInZipcode from '../API/GetNursingHomesInZipcode';
import GetGeriatricianInZipcode from '../API/GetGeriatricianInZipcode';
import ZipcodeAPIGetOne from '../API/ZipcodeAPIGetOne';

function InstancePage(props) {
  const { title } = useParams();
  const location = useLocation();
  const {
    id,
    model_name,
    img,
    attribute1_name,
    attribute1_value,
    attribute2_name,
    attribute2_value,
    attribute3_name,
    attribute3_value,
    attribute4_name,
    attribute4_value,
    attribute5_name,
    attribute5_value,
    attribute6_name,
    attribute6_value,
    attribute7_name,
    attribute7_value,
    attribute8_name,
    attribute8_value,
    attribute9_name,
    attribute9_value,
    zipcodes_id,
    latitude,
    longitude,
  } = location.state;

  const pageStyle = {
    padding: '5%',
    backgroundColor: "#2a596e",
  };
  const containerStyle = {
    display: "flex",
    justifyContent: "space-around",
    flexWrap: 'wrap',
    alignItems: 'center',
    maxHeight: '200%',
    padding: '5% 3% 5% 3%',
    backgroundColor: '#d79935',
  };

  const textStyle = {
    color: "black",
    fontSize: "150%",
    wordWrap: "break-word",
    paddingTop: '5%'
  }

  const modelsStyle = {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'start',
  }

  const mapStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '5%',
  }

  const [connection1, setConnection1] = useState([]);
  const [connection2, setConnection2] = useState([]);
  const [connection1_name, setConnection1Name] = useState('');
  const [connection2_name, setConnection2Name] = useState('');
  const [loaded, setLoaded] = useState(false); 

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (model_name === 'Zipcode') {
          setConnection1Name('Nursing Homes');
          setConnection2Name('Geriatricians');
          setConnection1(await GetNursingHomesInZipcode(zipcodes_id));
          setConnection2(await GetGeriatricianInZipcode(zipcodes_id));
        } else if (model_name === 'NursingHome') {
          console.log("zipcodes id", zipcodes_id);
          setConnection1Name('Zipcodes');
          setConnection2Name('Geriatricians');
          setConnection1(await ZipcodeAPIGetOne(zipcodes_id));
          setConnection2(await GetGeriatricianInZipcode(zipcodes_id));
        } else {
          setConnection1Name('Zipcodes');
          setConnection2Name('Nursing Homes');
          setConnection1(await ZipcodeAPIGetOne(zipcodes_id));
          setConnection2(await GetNursingHomesInZipcode(zipcodes_id));
        }
        setLoaded(true);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [model_name, zipcodes_id]);

  return (
    <div id={id} style={pageStyle}>
      <header style={{ display: 'flex', justifyContent: 'center', color: "white", paddingBottom: '2%' }}>
        <h1>{title}</h1>
      </header>
      <div style={{ color: "white", backgroundColor: "#2a596e" }}>
        <div style={containerStyle}>
          <Image src={img} className="h-2" style={{ display: 'flex', justifyContent: 'center', width: '60%', paddingRight: '10%' }} />
          <p style={textStyle}>
            <strong>{attribute1_name + ": "} </strong>{attribute1_value}
            <br></br>
            <br></br>
            <strong>{attribute2_name + ": "} </strong>{attribute2_value}
            <br></br>
            <br></br>
            <strong>{attribute3_name + ": "} </strong>{attribute3_value}
            <br></br>
            <br></br>
            <strong>{attribute4_name + ": "} </strong>{attribute4_value}
            <br></br>
            <br></br>
            <strong>{attribute5_name + ": "} </strong>{attribute5_value}
            <br></br>
            <br></br>
            <strong>{attribute6_name + ": "} </strong>{attribute6_value}
            <br></br>
            <br></br>
            <strong>{attribute7_name + ": "} </strong>{attribute7_value}
            <br></br>
            <br></br>
            <strong>{attribute8_name + ": "} </strong>{attribute8_value}
            <br></br>
            <br></br>
            <strong>{attribute9_name + ": "} </strong>{attribute9_value}
          </p>
        </div>
        <div style={mapStyle}>
          <Map latitude={latitude} longitude={longitude} />
        </div>

        {loaded ? (
        <>
          <div style={{ padding: '5% 0 0 0' }}>
            <h1>{connection1_name}</h1>
            <div style={modelsStyle}>
            {connection1?.map((connection) => {
              let connection1_title =
                connection1_name === "Nursing Homes"
                  ? connection.name
                  : connection1_name === "Zipcodes"
                    ? connection.zipcode
                    : connection.first_name + " " + connection.last_name;
              return (
                <div style={{paddingLeft: '2%'}}>
                  <SmallCard model={connection} model_type={connection1_name} model_title={connection1_title} model_img={connection.img} />
                </div>
              );
            })}
            </div>
          </div>

          <div style={{ padding: '5% 0 0 0' }}>
              <h1>{connection2_name}</h1>
              <div style={modelsStyle}>
                {connection2?.map((connection) => {
                  let connection2_title =
                    connection2_name === "Nursing Homes"
                      ? connection.name
                      : connection2_name === "Zipcodes"
                        ? connection.zipcode
                        : connection.first_name + " " + connection.last_name;
                  return (
                    <div style={{paddingLeft: '2%'}}>
                      <SmallCard model={connection} model_type={connection2_name} model_title={connection2_title} model_img={connection.img} />
                    </div>   
                  );
              })}
            </div>
          </div>
        </>
      ) : null} 
      </div>
    </div>
  );
}

export default InstancePage;
