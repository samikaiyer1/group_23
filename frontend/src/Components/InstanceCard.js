import React from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Highlighter from "react-highlight-words";


export default function InstanceCard(CardProps){
    const {
        id,
        model_name,
        title, 
        img,
        attribute1_name,
        attribute1_value,
        attribute2_name,
        attribute2_value,
        attribute3_name,
        attribute3_value,
        attribute4_name,
        attribute4_value,
        attribute5_name,
        attribute5_value,
        attribute6_name,
        attribute6_value,
        attribute7_name,
        attribute7_value,
        attribute8_name,
        attribute8_value,
        attribute9_name,
        attribute9_value,
        zipcodes_id,
        latitude,
        longitude,
        search,
      } = CardProps;

    const cardStyle = {
        width: '18rem',
        height: '100%',
        backgroundColor: "#d79935",
      };
    
    const handleLinkClick = () => {
      window.scrollTo(0, 0);
    };

    function highlightSearch(text) {
      if (search != null) {
        return (
          <Highlighter
            searchWords={search.split(" ")}
            autoEscape={true}
            textToHighlight={text}
          />
        );
      }
      return text;
    }

    return (
        <Card style={cardStyle}>
          <Card.Img variant="top" src={img} />
          <Card.Body>
            <Card.Title>{highlightSearch(title)}</Card.Title>
            <Card.Text>{attribute1_name + ': '}
            {highlightSearch("" + attribute1_value)}
            <br/>
            {attribute2_name + ': '}
            {highlightSearch("" + attribute2_value)}
            <br/>
            {attribute3_name + ': '}
            {highlightSearch("" + attribute3_value)}
            <br/>
            {attribute4_name + ': '}
            {highlightSearch("" + attribute4_value)}
            <br/>
            {attribute5_name + ': '}
            {highlightSearch("" + attribute5_value)}
            </Card.Text>
            <Link
              to={{pathname: `/${title}`}}
              state={{
                id,
                model_name,
                title, 
                img,
                attribute1_name,
                attribute1_value,
                attribute2_name,
                attribute2_value,
                attribute3_name,
                attribute3_value,
                attribute4_name,
                attribute4_value,
                attribute5_name,
                attribute5_value,
                attribute6_name,
                attribute6_value,
                attribute7_name,
                attribute7_value,
                attribute8_name,
                attribute8_value,
                attribute9_name,
                attribute9_value,
                zipcodes_id, 
                latitude,
                longitude
             }}

             onClick={handleLinkClick} 
            >
              <Button style={{ backgroundColor: '#2a596e', borderColor: '#2a596e' }}>
                More Info
              </Button>
            </Link>
          </Card.Body>
        </Card>
      );
    }