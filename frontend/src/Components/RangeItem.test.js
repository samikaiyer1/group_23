import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // for expect assertions

import RangeItem from './RangeItem';

describe('RangeItem Component', () => {
  test('renders correctly', () => {
    const { container } = render(<RangeItem values={[0, 10]} />);
    expect(container).toBeInTheDocument();
  });
});
