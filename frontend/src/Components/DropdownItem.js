import React, { useState } from 'react'
import Dropdown from 'react-bootstrap/Dropdown';
import FormCheck from 'react-bootstrap/FormCheck';

function updateFilters(adding, value, filters, setFilters, setPage){
    let filtersCopy = filters.slice();
    if(adding){
        filtersCopy.push(value);
    } else {
        filtersCopy = filtersCopy.filter(item => item !== value);
    }
    setFilters(filtersCopy);
    setPage(1);
}

export default function DropdownItem(props){
  const { value, filters, setFilters, setPage } = props;

  return (
    <Dropdown.Item
      onClick={e => {
        e.stopPropagation();
        const checkbox = e.currentTarget.querySelector('input');
        checkbox.click();
      }}
    >
      <FormCheck
        type="checkbox"
        label={value}
        onClick={e => {
          e.stopPropagation();
          updateFilters(e.currentTarget.checked, value, filters, setFilters, setPage);
        }}
      />
    </Dropdown.Item>
  );

      
}