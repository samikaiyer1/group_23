function fetchZipcodeData(zipCodeId) {
  const APIurl = `https://backend.goldenyearsguide.me/api/zipcodes/${zipCodeId}`;
  return fetch(APIurl)
    .then((response) => response.json())
    .catch((error) => {
      console.error('Error fetching zipcodes data:', error);
    });
}

export default fetchZipcodeData;
