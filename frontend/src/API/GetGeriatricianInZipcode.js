function GetGeriatricianInZipcode(zipcodeid) {
  const APIurl = `https://backend.goldenyearsguide.me/api/zipcode/get_geriatricians_in_zipcode/${zipcodeid}`;

  return fetch(APIurl)
    .then((response) => response.json())
    .catch((error) => {
      console.error('Error fetching geriatrician data:', error);
    });
}

export default GetGeriatricianInZipcode;
