import React, { useEffect, useState } from 'react';

function GeriatricianAPIGetOne() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(`https://backend.goldenyearsguide.me/api/geriatricians/${geriatricianId}`)
      .then((response) => response.json())
      .then((data) => {
        setData(data); 
      })
      .catch((error) => {
        console.error('Error fetching get one geriatricians data:', error);
      });
  }, []);

  return data;
}

export default GeriatricianAPIGetOne;