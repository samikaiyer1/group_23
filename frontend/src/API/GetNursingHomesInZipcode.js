function GetNursingHomesInZipcode(zipcodeid) {
  const APIurl = `https://backend.goldenyearsguide.me/api/zipcode/get_nursing_homes_in_zipcode/${zipcodeid}`;

  return fetch(APIurl)
    .then((response) => response.json())
    .catch((error) => {
      console.error('Error fetching nursing homes data:', error);
    });
}

export default GetNursingHomesInZipcode;