import { useEffect, useState } from 'react';

function GeriatriciansCount() {
  const [data, setData] = useState([]);
  const url = 'https://backend.goldenyearsguide.me/api/geriatricians_count';
  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setData(data); 
      })
      .catch((error) => {
        console.error('Error fetching the count geriatricians data:', error);
      });
  }, []);

  return data;
}

export default GeriatriciansCount;