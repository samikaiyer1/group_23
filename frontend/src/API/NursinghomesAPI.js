function NursingHomesAPI(page, cities, councils, beds, rating, health, sort_type, desc, search) {
  let endUrl="";
  if(cities.length > 0){
    const cityString = cities.join(",");
    endUrl += '&city=' + cityString;
  }

  if(councils.length > 0){
    const councilString = councils.join(",");
    endUrl += '&council=' + councilString;
  }

  if(beds[0] !== '' && beds[0] !== undefined){
    endUrl += '&bedsmin=' + beds[0];
  }

  if(beds[1] !== '' && beds[1] !== undefined){
    endUrl += '&bedsmax=' + beds[1];
  }

  if(rating[0] !== '' && rating[0] !== undefined){
    endUrl += '&ratingmin=' + rating[0];
  }

  if(rating[1] !== '' && rating[1] !== undefined){
    endUrl += '&ratingmax=' + rating[1];
  }

  if(health[0] !== '' && health[0] !== undefined){
    endUrl += '&healthmin=' + health[0];
  }

  if(health[1] !== '' && health[1] !== undefined){
    endUrl += '&healthmax=' + health[1];
  }
  
  if (sort_type !== '' && sort_type !== undefined) {
    endUrl += '&sort_type=' + sort_type;
  }

  if (desc !== '' && desc !== undefined) {
    endUrl += '&desc=' + desc;
  }

  if (search !== '' && search !== undefined) {
    endUrl += '&search=' + encodeURIComponent(search);
  }

  const url = 'https://backend.goldenyearsguide.me/api/nursing_homes?page=' + page + endUrl;
  return fetch(url)
    .then((response) => response.json())
    .catch((error) => {
      console.error('Error fetching nursing homes data:', error);
    });
}

export default NursingHomesAPI;
