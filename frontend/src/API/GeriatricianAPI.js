function GeriatricianAPI(page, names, cities, genders, schools, years, sort_type, desc, search) {
  let endUrl="";
  if(names.length > 0){
    const namesString = names.join(">");
    endUrl += '&facility=' + namesString;
  }

  if(cities.length > 0){
    const cityString = cities.join(",");
    endUrl += '&city=' + cityString;
  }

  if(genders.length > 0){
    const gendersString = genders.join(",");
    endUrl += '&gender=' + gendersString;
  }

  if(schools.length > 0){
    const schoolsString = schools.join(",");
    endUrl += '&medical_school=' + schoolsString;
  }

  if(years[0] !== '' && years[0] !== undefined){
    endUrl += '&gradyearmin=' + years[0];
  }

  if(years[1] !== '' && years[1] !== undefined){
    endUrl += '&gradyearmax=' + years[1];
  }

  if (sort_type !== '' && sort_type !== undefined) {
    endUrl += '&sort_type=' + sort_type;
  }

  if (desc !== '' && desc !== undefined) {
    endUrl += '&desc=' + desc;
  }

  if (search !== '' && search !== undefined) {
    endUrl += '&search=' + encodeURIComponent(search);
  }

  const url = 'https://backend.goldenyearsguide.me/api/geriatricians?page=' + page + endUrl;
  return fetch(url)
    .then((response) => response.json())
    .catch((error) => {
      console.error('Error fetching geriatricians data:', error);
    });
}

export default GeriatricianAPI;
