import { useEffect, useState } from 'react';

function ZipcodesCount() {
  const [data, setData] = useState([]);
  const url = 'https://backend.goldenyearsguide.me/api/zipcodes_count';
  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setData(data); 
      })
      .catch((error) => {
        console.error('Error fetching the count zipcodes data:', error);
      });
  }, []);

  return data;
}

export default ZipcodesCount;
