function ZipcodeAPI(page, cities, counties, population, ages, income, sort_type, desc, search) {
  let endUrl="";
  if(cities.length > 0){
    const cityString = cities.join(",");
    endUrl += '&city=' + cityString;
  }

  if(counties.length > 0){
    const countyString = counties.join(",");
    endUrl += '&county=' + countyString;
  }

  if(population[0] !== '' && population[0] !== undefined){
    endUrl += '&populationmin=' + population[0];
  }

  if(population[1] !== '' && population[1] !== undefined){
    endUrl += '&populationmax=' + population[1];
  }

  if(ages[0] !== '' && ages[0] !== undefined){
    endUrl += '&agemin=' + ages[0];
  }

  if(ages[1] !== '' && ages[1] !== undefined){
    endUrl += '&agemax=' + ages[1];
  }

  if(income[0] !== '' && income[0] !== undefined){
    endUrl += '&incomemin=' + income[0];
  }

  if(income[1] !== '' && income[1] !== undefined){
    endUrl += '&incomemax=' + income[1];
  }

  if (sort_type !== '' && sort_type !== undefined) {
    endUrl += '&sort_type=' + sort_type;
  }

  if (desc !== '' && desc !== undefined) {
    endUrl += '&desc=' + desc;
  }

  if (search !== '' && search !== undefined) {
    endUrl += '&search=' + encodeURIComponent(search);
  }
  
  const url = 'https://backend.goldenyearsguide.me/api/zipcodes?page=' + page + endUrl;

  return fetch(url)
    .then((response) => response.json())
    .catch((error) => {
      console.error('Error fetching zipcodes data:', error);
    });
}

export default ZipcodeAPI;