import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function NavBar() {
  const navbarStyle = {
    paddingTop: '2%',
    paddingBottom: '2%',
    backgroundColor: "#91c2d4",
  };

  return (
    <Navbar style={navbarStyle} expand="xl">
      <Container>
        <Navbar.Brand style={{ fontSize: '25px', paddingRight: '15px' }}>Golden Years Guide</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/">Home</Nav.Link>
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/About">About</Nav.Link>
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/ZipCodes">Zipcodes</Nav.Link>
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/NursingHomes">Nursing Homes</Nav.Link>
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/Geriatricians">Geriatricians</Nav.Link>
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/Visualizations">Visualizations</Nav.Link>
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/ProviderVisualizations">Provider Visualizations</Nav.Link>
            <Nav.Link style={{ fontSize: '20px', paddingRight: '15px', whiteSpace: 'nowrap' }} as={Link} to="/SearchPage">Search</Nav.Link>
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
