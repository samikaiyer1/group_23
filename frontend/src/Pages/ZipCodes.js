import React, { useEffect, useState } from 'react';
import InstanceCard from '../Components/InstanceCard';
import ZipcodeAPI from '../API/ZipcodeAPI';
import { Button, ButtonGroup } from 'react-bootstrap';
import ZipcodesFilters from '../Components/ZipcodesFilters';

export default function ZipCodes(){
    const [zipcodes_count, setCount] = useState([]);
    const total_pages = Math.ceil(zipcodes_count / 20);
    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
        paddingBottom: '5%',
        flexWrap: 'wrap',
        gap: '2%'
      };

    const pageStyle = {
        padding: '5%',
        backgroundColor: "#2a596e",
    };

    const [zipcodes_data, setData] = useState([]);
    const [curpage, setPage] = useState(1);
    const [sortName, setSortName] = useState('Sort');
    const [descName, setDescName] = useState('Ascending');
    const [sort, setSort] = useState('');
    const [desc, setDesc] = useState(0);
    const [selectedCities, setSelectedCities] = useState([]);
    const [selectedCounties, setSelectedCounties] = useState([]);
    const [populationValues, setPopulationValues] = useState(['', '']);
    const [ageValues, setAgeValues] = useState(['', '']);
    const [incomeValues, setIncomeValues] = useState(['', '']);
    const [searchTerm, setSearchTerm] = useState('');
    
    useEffect(() => {
        const fetchDataforPage = async (curpage, selectedCities, selectedCounties, populationValues, ageValues, incomeValues, sort, desc, search) => {
            try {
                const data = await ZipcodeAPI(curpage, selectedCities, selectedCounties, populationValues, ageValues, incomeValues, sort, desc, search);
                setData(data.data);
                setCount(data.count)
            } catch (error) {
                console.error('Error fetching zipcode data on call:', error);
            }
          };
          fetchDataforPage(curpage, selectedCities, selectedCounties, populationValues, ageValues, incomeValues, sort, desc, searchTerm);
    }, [curpage, selectedCities, selectedCounties, populationValues, ageValues, incomeValues, sort, desc, searchTerm]);

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
        setPage(1);
    };

    let items= []
    const buttonStyles = {
        backgroundColor: "#91c2d4",
        color: 'black',
    };
    const group = {
        display: 'block',
        textAlign: 'center',
        paddingBottom: '2%'
    }
    for (let number = 1; number <= total_pages; number++) {
        if (curpage == number){
            items.push(
                <Button variant="secondary" key={number} onClick={() => setPage(number)}>
                  {number}
                </Button>,
            );
        } else {
            items.push(
                <Button style={buttonStyles} key={number} onClick={() => setPage(number)}>
                  {number}
                </Button>,
            );
        } 
    }

    return(
        <div style={pageStyle}>
            <div style={{ color: 'white' }}>
                <h2>{"Total Number of Zipcodes: " + String(zipcodes_count)}</h2>
                <div style={{paddingTop: '1%', paddingBottom:'1%'}}>
                    <ZipcodesFilters 
                        sortName={sortName}
                        setSortName={setSortName}
                        setSort={setSort}
                        sortAttribs={sortAttribs}
                        descName={descName}
                        setDescName={setDescName}
                        setDesc={setDesc}
                        descAttribs={descAttribs}
                        selectedCities={selectedCities}
                        setSelectedCities={setSelectedCities}
                        selectedCounties={selectedCounties}
                        setSelectedCounties={setSelectedCounties}
                        populationValues={populationValues}
                        setPopulationValues={setPopulationValues}
                        ageValues={ageValues}
                        setAgeValues={setAgeValues}
                        incomeValues={incomeValues}
                        setIncomeValues={setIncomeValues}
                        setPage={setPage}
                     />
                </div> 
                
                <div style={{ display: 'flex', justifyContent: 'center', paddingBottom: '2%'}}>
                    <input
                        type="text"
                        placeholder="Search Zipcodes"
                        value={searchTerm}
                        onChange={handleSearchChange}
                        style={{
                        width: '300px',
                        height: '40px',
                        fontSize: '16px',
                        textAlign: 'center',
                        }}
                    />
                </div>
                <ButtonGroup style={group}>{items}</ButtonGroup>
            </div>
            <div style={containerStyle}>
            {zipcodes_data?.map (zipcode => (
                    <div style = {{paddingBottom: '5%'}}>
                        <InstanceCard
                        id = {zipcode.id}
                        model_name = "Zipcode"
                        title = {zipcode.zipcode}
                        img= {zipcode.img}
                        attribute1_name = "City"
                        attribute1_value = {zipcode.city}
                        attribute2_name = "County"
                        attribute2_value = {zipcode.county}
                        attribute3_name = "Population"
                        attribute3_value = {zipcode.population}
                        attribute4_name = "Median Age"
                        attribute4_value = {zipcode.median_age}
                        attribute5_name = "Average Income per Household"
                        attribute5_value = {zipcode.avg_income_household}
                        attribute6_name = "Latitude"
                        attribute6_value = {zipcode.latitude}
                        attribute7_name = "Longitude"
                        attribute7_value = {zipcode.longitude}
                        attribute8_name = "Number of Households"
                        attribute8_value = {zipcode.households}
                        attribute9_name = "Persons per Household"
                        attribute9_value = {zipcode.prsn_per_household}
                        latitude={zipcode.latitude}
                        longitude={zipcode.longitude} 
                        zipcodes_id = {zipcode.id}
                        search = {searchTerm}
                     />
                    </div>     
                ))}
            </div>
        </div>
    );
}

const sortAttribs = [
    ["Sort", undefined],
    ["Zipcode", "zipcode"],
    ["City", "city"],
    ["County", "county"],
    ["Population", "population"],
    ["Median Age", "median_age"],
    ["Average Income", "avg_income_household"]
];

const descAttribs = [
    ["Ascending", "0"],
    ["Descending", "1"],
];

const cities = [
    "FRISCO",
    "CARROLLTON",
    "ALLEN",
    "PLANO",
    "HALTOM CITY",
    "GRAPEVINE",
    "DALLAS",
    "FORT WORTH",
    "RICHARDSON",
    "KELLER",
    "MCKINNEY"
  ];

  const counties = [
    "TARRANT",
    "COLLIN",
    "DENTON",
    "DALLAS"
  ];
