import React, { useEffect, useState } from 'react';
import InstanceCard from '../Components/InstanceCard';
import NursingHomesAPI from '../API/NursinghomesAPI';
import { Button, ButtonGroup } from 'react-bootstrap';
import NursingHomesFilters from '../Components/NursingHomesFilters';

export default function NursingHomes(){
    const [nursing_homes_count, setCount] = useState([]);
    const total_pages = Math.ceil(nursing_homes_count / 20);
    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
        paddingBottom: '5%',
        flexWrap: 'wrap',
        gap: '2%'
      };

      const pageStyle = {
        padding: '5%',
        backgroundColor: "#2a596e",
      };

    const [nursing_homes_data, setData] = useState([]);
    const [curpage, setPage] = useState(1);
    const [sortName, setSortName] = useState('Sort');
    const [descName, setDescName] = useState('Ascending');
    const [sort, setSort] = useState('');
    const [desc, setDesc] = useState(0);
    const [selectedCities, setSelectedCities] = useState([]);
    const [selectedCouncils, setSelectedCouncils] = useState([]);
    const [bedsValues, setBedsValues] = useState(['', '']);
    const [ratingValues, setRatingValues] = useState(['', '']);
    const [healthValues, setHealthValues] = useState(['', '']);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        const fetchDataforPage = async (curpage, selectedCities, selectedCouncils, bedsValues, ratingValues, healthValues, sort, desc, search) => {
            try {
                const data = await NursingHomesAPI(curpage, selectedCities, selectedCouncils, bedsValues, ratingValues, healthValues, sort, desc, search);
                setData(data.data);
                setCount(data.count);
            } catch (error) {
                console.error('Error fetching nursing home data on call:', error);
            }
          };
          fetchDataforPage(curpage, selectedCities, selectedCouncils, bedsValues, ratingValues, healthValues, sort, desc, searchTerm);
    }, [curpage, selectedCities, selectedCouncils, bedsValues, ratingValues, healthValues, sort, desc, searchTerm]);

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
        setPage(1);
    };

    let items= []
    
    const buttonStyles = {
        backgroundColor: "#91c2d4",
        color: 'black',
    };
    const group = {
        display: 'block',
        textAlign: 'center',
        paddingBottom: '2%'
    }
    for (let number = 1; number <= total_pages; number++) {
        if (curpage === number){
            items.push(
                <Button variant="secondary" key={number} onClick={() => setPage(number)}>
                  {number}
                </Button>,
            );
        } else {
            items.push(
                <Button style={buttonStyles} key={number} onClick={() => setPage(number)}>
                  {number}
                </Button>,
            );
        } 
    }

    return(
        <div style={pageStyle}>
            <div style={{ color: 'white' }}>
                <h2>{"Total Number of Nursing Homes: " + String(nursing_homes_count)}</h2>
                <div style={{paddingTop: '1%', paddingBottom: '1%'}}>
                    <NursingHomesFilters
                        sortName={sortName}
                        setSortName={setSortName}
                        setSort={setSort}
                        sortAttribs={sortAttribs}
                        descName={descName}
                        setDescName={setDescName}
                        setDesc={setDesc}
                        descAttribs={descAttribs} 
                        selectedCities={selectedCities}
                        setSelectedCities={setSelectedCities}
                        selectedCouncils={selectedCouncils}
                        setSelectedCouncils={setSelectedCouncils}
                        bedsValues={bedsValues}
                        setBedsValues={setBedsValues}
                        ratingValues={ratingValues}
                        setRatingValues={setRatingValues}
                        healthValues={healthValues}
                        setHealthValues={setHealthValues}
                        setPage={setPage}
                    />
                </div> 

                <div style={{ display: 'flex', justifyContent: 'center', paddingBottom: '2%'}}>
                    <input
                        type="text"
                        placeholder="Search Nursing Homes"
                        value={searchTerm}
                        onChange={handleSearchChange}
                        style={{
                        width: '300px',
                        height: '40px',
                        fontSize: '16px',
                        textAlign: 'center',
                        }}
                    />
                </div>
                <ButtonGroup style={group}>{items}</ButtonGroup>
            </div>
            <div style={containerStyle}>
            {nursing_homes_data?.map (nursing_home => (
                    <div style = {{paddingBottom: '5%'}}>
                        <InstanceCard key = {nursing_home.id}
                        id = {nursing_home.id}
                        model_name = "NursingHome"
                        title = {nursing_home.name}
                        img = {nursing_home.img}
                        attribute1_name = "City"
                        attribute1_value = {nursing_home.city}
                        attribute2_name = "Council"
                        attribute2_value = {nursing_home.council}
                        attribute3_name = "Number of Certified Beds"
                        attribute3_value = {nursing_home.num_beds}
                        attribute4_name = "Overall Rating(Out of 5)"
                        attribute4_value = {nursing_home.overall_rating}
                        attribute5_name = "Health Inspection Rating(Out of 5)"
                        attribute5_value = {nursing_home.health_rating}
                        attribute6_name = "Number of Residents per Day"
                        attribute6_value = {nursing_home.num_residents}
                        attribute7_name = "Legal Business Name"
                        attribute7_value = {nursing_home.business_name}
                        attribute8_name = "Address"
                        attribute8_value = {nursing_home.address}
                        attribute9_name = "Zipcode"
                        attribute9_value = {nursing_home.zipcode}
                        latitude={nursing_home.latitude}
                        longitude={nursing_home.longitude} 
                        zipcodes_id = {nursing_home.zipcode_ptr}
                        search = {searchTerm}
                     />
                    </div>
                ))}
            </div>
        </div>
    );
}

const sortAttribs = [
  ["Sort", undefined],
  ["Name", "name"],
  ["Address", "address"],
  ["City", "city"],
  ["Zipcode", "zipcode"],
  ["Beds", "num_beds"],
  ["Rating", "overall_rating"],
  ["Health Rating", "health_rating"],
  ["Latitude", "latitude"],
  ["Longitude", "longitude"]
];

const descAttribs = [
    ["Ascending", "0"],
    ["Descending", "1"],
];

const cities = [
    "DALLAS",
    "FORT WORTH",
    "RICHARDSON",
    "KELLER",   
    "MCKINNEY",
    "FRISCO",
    "CARROLLTON",
    "ALLEN",
    "WATAUGA",
    "RICHLAND HILLS",
    "PLANO",
    "WHITE SETTLEMENT",
    "GRAPEVINE"
];

const councils = [
    "Both",
    "Resident",
    "Neither"
];