import { React, useState, useEffect } from 'react';
import {Button, ButtonGroup} from 'react-bootstrap';
import InstanceCard from '../Components/InstanceCard';

export default function SearchPage(){

    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [curpage, setPage] = useState(1);
    const [search_count, setCount] = useState([]);
    const total_pages = Math.ceil(search_count / 20);

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
        setPage(1);
    };

    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
        paddingBottom: '5%',
        flexWrap: 'wrap',
        gap: '2%'
    };

    const pageStyle = {
        padding: '5%',
        backgroundColor: "#2a596e",
    };

    let items = []

    const buttonStyles = {
        backgroundColor: "#91c2d4",
        color: 'black',
    };

    const group = {
        display: 'block',
        textAlign: 'center',
        paddingBottom: '2%'
    }

    for (let number = 1; number <= total_pages; number++) {
        items.push(
            <Button style={buttonStyles} key={number} onClick={() => setPage(number)}>
              {number}
            </Button>,
        );
    }

    useEffect(() => {
        const fetchData = async (curpage, searchTerm) => {
          const apiUrl = `https://backend.goldenyearsguide.me/api/search?search=${encodeURIComponent(searchTerm)}&page=${curpage}`;
          try {
            const response = await fetch(apiUrl);
            const result = await response.json();
            setSearchResults(result.data);
            setCount(result.count)
          } catch (error) {
            console.error('Error:', error);
          }
        };
    
        fetchData(curpage, searchTerm);
      }, [curpage, searchTerm]);

    return (
        <div style={pageStyle}>
            <div style={{ color: 'white' }}>
                <div style={{ display: 'flex', justifyContent: 'center', paddingBottom: '5%' }}>
                    <input
                        type="text"
                        placeholder="Search website"
                        value={searchTerm}
                        onChange={handleSearchChange}
                        style={{
                            width: '300px',
                            height: '40px',
                            fontSize: '16px',
                            textAlign: 'center',
                        }}
                    />
                </div>
                <ButtonGroup style={group}>{items}</ButtonGroup>
                
            </div>
            {search_count > 0 && (
                    <h2 style={{ color: 'white', paddingBottom: '1%' }}>{"Number of Results: " + String(search_count)}</h2>
                )}
            <div style={containerStyle}>
                {searchResults?.map(result => {
                    if (result.type === 'nursing_home') {
                        return (
                            <div style={{ paddingBottom: '5%' }} key={result.data.id}>
                                <InstanceCard
                                    id={result.data.id}
                                    model_name="NursingHome"
                                    title={result.data.name}
                                    img={result.data.img}
                                    attribute1_name="City"
                                    attribute1_value={result.data.city}
                                    attribute2_name="Council"
                                    attribute2_value={result.data.council}
                                    attribute3_name="Number of Certified Beds"
                                    attribute3_value={result.data.num_beds}
                                    attribute4_name="Overall Rating(Out of 5)"
                                    attribute4_value={result.data.overall_rating}
                                    attribute5_name="Health Inspection Rating(Out of 5)"
                                    attribute5_value={result.data.health_rating}
                                    attribute6_name="Number of Residents per Day"
                                    attribute6_value={result.data.num_residents}
                                    attribute7_name="Legal Business Name"
                                    attribute7_value={result.data.business_name}
                                    attribute8_name="Address"
                                    attribute8_value={result.data.address}
                                    attribute9_name="Zipcode"
                                    attribute9_value={result.data.zipcode}
                                    latitude={result.data.latitude}
                                    longitude={result.data.longitude}
                                    zipcodes_id={result.data.zipcode_ptr}
                                    search={searchTerm}
                                />
                            </div>
                        );
                    } else if (result.type === 'geriatrician') {
                        // Render Geriatrician instance card
                        return (
                            <div style={{ paddingBottom: '5%' }} key={result.data.id}>
                                <InstanceCard
                                    id = {result.data.id}
                                    model_name = "Geriatrician"
                                    title = {result.data.first_name + " " + result.data.last_name}
                                    img= {result.data.img}
                                    attribute1_name = "Facility Name"
                                    attribute1_value = {result.data.facility_name}
                                    attribute2_name = "City"
                                    attribute2_value = {result.data.city}
                                    attribute3_name = "Gender"
                                    attribute3_value = {result.data.gender}
                                    attribute4_name = "Medical School"
                                    attribute4_value = {result.data.medical_school}
                                    attribute5_name = "Graduation Year"
                                    attribute5_value = {result.data.grad_year}
                                    attribute6_name = "Address"
                                    attribute6_value = {result.data.address}
                                    attribute7_name = "Number of Members in Organization"
                                    attribute7_value = {result.data.num_members}
                                    attribute8_name = "Secondary Specialization"
                                    attribute8_value = {result.data.secondary_specialization}
                                    attribute9_name = "Zipcode"
                                    attribute9_value = {result.data.zipcode}
                                    latitude={result.data.latitude}
                                    longitude={result.data.longitude} 
                                    zipcodes_id = {result.data.zipcodes_id}
                                    search = {searchTerm}
                                />
                            </div>
                        );
                    } else if (result.type === 'zipcode') {
                        // Render Zipcode instance card
                        return (
                            <div style={{ paddingBottom: '5%' }} key={result.data.id}>
                                <InstanceCard
                                    id = {result.data.id}
                                    model_name = "Zipcode"
                                    title = {result.data.zipcode}
                                    img= {result.data.img}
                                    attribute1_name = "City"
                                    attribute1_value = {result.data.city}
                                    attribute2_name = "County"
                                    attribute2_value = {result.data.county}
                                    attribute3_name = "Population"
                                    attribute3_value = {result.data.population}
                                    attribute4_name = "Median Age"
                                    attribute4_value = {result.data.median_age}
                                    attribute5_name = "Average Income per Household"
                                    attribute5_value = {result.data.avg_income_household}
                                    attribute6_name = "Latitude"
                                    attribute6_value = {result.data.latitude}
                                    attribute7_name = "Longitude"
                                    attribute7_value = {result.data.longitude}
                                    attribute8_name = "Number of Households"
                                    attribute8_value = {result.data.households}
                                    attribute9_name = "Persons per Household"
                                    attribute9_value = {result.data.prsn_per_household}
                                    latitude={result.data.latitude}
                                    longitude={result.data.longitude} 
                                    zipcodes_id = {result.data.id}
                                    search = {searchTerm}
                                />
                            </div>
                        );
                    }
                })}
                </div>
        </div>
    );
}