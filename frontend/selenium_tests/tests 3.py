import unittest
from selenium import webdriver

class SeleniumTests(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.get('http://localhost:3000')
        self.driver.implicitly_wait(10)
        return super().setUp()
    
    def tearDown(self) -> None:
        self.driver.quit()
        return super().tearDown()
    
    def test_title(self):
        self.assertEqual(self.driver.title, 'React App')
