versions:
	npm --version
	@echo "this should be 10.2.0"

run_frontend:
	cd frontend; \
		docker build -t frontend:latest .; \
		docker run frontend:latest

run_backend:
	@echo "Make sure to run the following commands after!"
	@echo "cd /usr/python/app"
	@echo "gunicorn -w 2 -b 0.0.0.0:8080 '__init__:app'"
	cd backend; \
		docker build -t backend:latest .; \
		docker run --rm --network=host -i -t -v /home/ubuntu/group_23/backend/:/usr/python backend

clean:
	rm -rf node_modules/

pull:
	make clean
	@echo
	git pull
	git status

all:
	make pull 
	make run
