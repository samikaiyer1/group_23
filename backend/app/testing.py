import requests


# API endpoint URL
url = "https://api.unsplash.com/photos/?client_id=1Xsdn4lt1HgZJx-B9ZdajoRjF11P_CKMRmCNrNRS8tA&per_page=2&query=school"

# Make the GET request
response = requests.get(url)

# Check if the request was successful (status code 200)
if response.status_code == 200:
    # Parse and work with the JSON response
    data = response.json()
    # Do something with the response data here
    print(data)
else:
    # Handle the error gracefully
    print(f'Error: {response.status_code} - {response.text}')