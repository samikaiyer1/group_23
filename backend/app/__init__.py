from flask import Flask
from flask_cors import CORS
import sys
import os
from os.path import join, dirname
from dotenv import load_dotenv
from flask_sqlalchemy import SQLAlchemy

# .env configuration
dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)

DATABASE_URI = "postgresql://group23:group23password@goldenyearsguide.cr5rrg3llfog.us-east-1.rds.amazonaws.com:5432/goldenyearsguide"

# Configure app
app = Flask(__name__)
CORS(app)
app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

import routes
