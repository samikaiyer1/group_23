from __init__ import app
import unittest

import json

class PythonUnitTests(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        self.client = app.test_client()

    def testGetZipcodeCount(self):
        with self.client:
            response = self.client.get("/api/zipcodes_count")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(data, 51)
            
    def testGetAllZipcode(self):
        with self.client:
            response = self.client.get("/api/zipcodes")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            self.assertEqual(len(data), 51)
            
    def testGetGeriatricianCount(self):
        with self.client:
            response = self.client.get("/api/geriatricians_count")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(data, 151)
            
    def testGetAllGeriatrician(self):
        with self.client:
            response = self.client.get("/api/geriatricians")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            self.assertEqual(len(data), 151)
            
    def testGetNursingHomeCount(self):
        with self.client:
            response = self.client.get("/api/nursing_homes_count")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)
            self.assertEqual(data, 96)
            
    def testGetAllNursingHome(self):
        with self.client:
            response = self.client.get("/api/nursing_homes")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            self.assertEqual(len(data), 96)

    def testGetEachZipcode(self):
        with self.client:
            response = self.client.get("/api/zipcodes")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for zipcode in data:
                n_response = self.client.get(f"/api/zipcodes/{zipcode['id']}")
                self.assertEqual(n_response.status_code, 200)
                n_data = json.loads(n_response.data)
                self.assertEqual(n_data[0], zipcode)
                
    def testGetEachNursingHome(self):
        with self.client:
            response = self.client.get("/api/nursing_homes")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for nursing_home in data:
                n_response = self.client.get(f"/api/nursing_homes/{nursing_home['id']}")
                self.assertEqual(n_response.status_code, 200)
                n_data = json.loads(n_response.data)
                self.assertEqual(n_data[0], nursing_home)
                
    def testGetEachGeriatrician(self):
        with self.client:
            response = self.client.get("/api/geriatricians")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for geriatrician in data:
                n_response = self.client.get(f"/api/geriatricians/{geriatrician['id']}")
                self.assertEqual(n_response.status_code, 200)
                n_data = json.loads(n_response.data)
                self.assertEqual(n_data[0], geriatrician)
    
    def testGetGeriatriciansInZipcode(self):
        with self.client:
            response = self.client.get("api/zipcodes")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for zipcode in data:
                n_response = self.client.get(f"/api/zipcode/get_geriatricians_in_zipcode/{zipcode['id']}")
                self.assertEqual(n_response.status_code, 200)
                n_data = json.loads(n_response.data)
                for geriatrician in n_data:
                    self.assertEqual(geriatrician['zipcode'][0:5], zipcode['zipcode'])
                    
    def testGetNursingHomesInZipcode(self):
        with self.client:
            response = self.client.get("api/zipcodes")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for zipcode in data:
                n_response = self.client.get(f"/api/zipcode/get_nursing_homes_in_zipcode/{zipcode['id']}")
                self.assertEqual(n_response.status_code, 200)
                n_data = json.loads(n_response.data)
                for nursing_home in n_data:
                    self.assertEqual(nursing_home['zipcode'][0:5], zipcode['zipcode'])
    
    def testGetGeriatriciansInNursingHomesZipcode(self):
        with self.client:
            response = self.client.get("api/nursing_homes")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for nursing_home in data:
                n_response = self.client.get(f"/api/nursing_home/get_close_geriatricians/{nursing_home['id']}")
                self.assertEqual(n_response.status_code, 200)
                n_data = json.loads(n_response.data)
                for geriatrician in n_data:
                    self.assertEqual(geriatrician['zipcode'][0:5], nursing_home['zipcode'])
    
    def testGetNursingHomesInGeriatriciansZipcode(self):
        with self.client:
            response = self.client.get("api/geriatricians")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for geriatrician in data:
                n_response = self.client.get(f"/api/geriatricians/get_close_nursing_homes/{geriatrician['id']}")
                self.assertEqual(n_response.status_code, 200)
                n_data = json.loads(n_response.data)
                for nursing_home in n_data:
                    self.assertEqual(geriatrician['zipcode'][0:5], nursing_home['zipcode'])
    
    def testSortedZipcodeByZipcode(self):
        with self.client:
            response = self.client.get("api/zipcodes?sort_type=zipcode")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for i in range(len(data) - 1):
                self.assertLessEqual(data[i]['zipcode'], data[i + 1]['zipcode'])
                
    def testSortedZipcodeByZipcodeDesc(self):
        with self.client:
            response = self.client.get("api/zipcodes?sort_type=zipcode&desc=1")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for i in range(len(data) - 1):
                self.assertGreaterEqual(data[i]['zipcode'], data[i + 1]['zipcode'])
    
    def testSortedGeriatriciansByZipcode(self):
        with self.client:
            response = self.client.get("api/geriatricians?sort_type=zipcode")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for i in range(len(data) - 1):
                self.assertLessEqual(data[i]['zipcode'], data[i + 1]['zipcode'])
                
    def testSortedGeriatriciansByZipcodeDesc(self):
        with self.client:
            response = self.client.get("api/geriatricians?sort_type=zipcode&desc=1")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for i in range(len(data) - 1):
                self.assertGreaterEqual(data[i]['zipcode'], data[i + 1]['zipcode'])
                
    def testSortedNursingHomesByZipcode(self):
        with self.client:
            response = self.client.get("api/nursing_homes?sort_type=zipcode")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for i in range(len(data) - 1):
                self.assertLessEqual(data[i]['zipcode'], data[i + 1]['zipcode'])
                
    def testSortedGeriatriciansByZipcodeDesc(self):
        with self.client:
            response = self.client.get("api/nursing_homes?sort_type=zipcode&desc=1")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            for i in range(len(data) - 1):
                self.assertGreaterEqual(data[i]['zipcode'], data[i + 1]['zipcode'])
    
    def testFilteredZipcodeInDallas(self):
        with self.client:
            response = self.client.get("api/zipcodes?city=DALLAS")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            self.assertGreater(len(data), 0)
            for i in range(len(data)):
                self.assertEqual(data[i]['city'], 'DALLAS')
                
    def testFilteredGeriatriciansInDallas(self):
        with self.client:
            response = self.client.get("api/geriatricians?city=DALLAS")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            self.assertGreater(len(data), 0)
            for i in range(len(data)):
                self.assertEqual(data[i]['city'], 'DALLAS')
                
    def testFilteredNursingHomesInDallas(self):
        with self.client:
            response = self.client.get("api/nursing_homes?city=DALLAS")
            self.assertEqual(response.status_code, 200)
            data = json.loads(response.data)['data']
            self.assertGreater(len(data), 0)
            for i in range(len(data)):
                self.assertEqual(data[i]['city'], 'DALLAS')

    def testZipcodeSearchAllen(self):
        with self.client:
            response = self.client.get("api/zipcodes?search=allen")
            self.assertEqual(response.status_code, 200)
            count = json.loads(response.data)['count']
            self.assertEqual(count, 2)
            
    def testZipcodeSearchAllenCollin(self):
        with self.client:
            response = self.client.get("api/zipcodes?search=allen%20collin")
            self.assertEqual(response.status_code, 200)
            count = json.loads(response.data)['count']
            self.assertEqual(count, 10)
            data = json.loads(response.data)['data']
            for zipcode in data[:2]:
                self.assertEqual(zipcode['city'], 'ALLEN')
                self.assertEqual(zipcode['county'], 'COLLIN')

    def testGeriatricianSearchDina(self):
        with self.client:
            response = self.client.get("api/geriatricians?search=dina")
            self.assertEqual(response.status_code, 200)
            count = json.loads(response.data)['count']
            self.assertEqual(count, 2)

    def testNursingHomeSearch195(self):
        with self.client:
            response = self.client.get("api/nursing_homes?search=195")
            self.assertEqual(response.status_code, 200)
            count = json.loads(response.data)['count']
            self.assertEqual(count, 2)

    def testSearchAllAllen(self):
        with self.client:
            response = self.client.get("api/search?search=allen")
            self.assertEqual(response.status_code, 200)
            count = json.loads(response.data)['count']
            self.assertEqual(count, 11)
if __name__ == "__main__":
    unittest.main()
