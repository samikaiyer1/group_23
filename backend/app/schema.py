from __init__ import app
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from models import NursingHomes, Geriatricians, Zipcodes

class nursing_home_schema(SQLAlchemyAutoSchema):
    class Meta:
        model = NursingHomes
        include_relationships = True
class geriatrician_schema(SQLAlchemyAutoSchema):
    class Meta:
        model = Geriatricians
        include_relationships = True
        include_fk = True
class geriatrician_schema(SQLAlchemyAutoSchema):
    class Meta:
        model = Geriatricians
        include_fk = True
class zipcode_schema(SQLAlchemyAutoSchema):
    class Meta:
        model = Zipcodes
        
NursingHomeSchema = nursing_home_schema()
NursingHomesSchema = nursing_home_schema(many=True)
        
GeriatricianSchema = geriatrician_schema()
GeriatriciansSchema = geriatrician_schema(many=True)

ZipcodeSchema = zipcode_schema()
ZipcodesSchema = zipcode_schema(many=True)