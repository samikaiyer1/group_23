from __init__ import db
from __init__ import app
import requests
import json
from Approved import approved_list
from models import Geriatricians, Zipcodes

def add_g_to_db():
    with app.app_context():
        url = "https://data.cms.gov/provider-data/api/1/datastore/query/mj5m-pzi6/0"

        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }
        geriatricians_data = []
        db.session.query(Geriatricians).delete()

        for zipcode in approved_list:
            zipcode = str(zipcode)
            payload = {
                "conditions": [
                    {
                        "resource": "t",
                        "property": "zip_code",
                        "value": zipcode,
                        "operator": "starts with"
                    },
                ],
                "limit": 3
            }
            zipcode_o = db.session.query(Zipcodes).filter(Zipcodes.zipcode == zipcode).all()[0]
            response = requests.post(url, headers=headers, json=payload)
            if response.status_code == 200:
                data = response.json()
                data_list = data.get("results")
                geriatricians_data += data_list
                for results in data_list:
                    sec_spec = results["sec_spec_1"]
                    grdyr = results["grd_yr"]
                    nummem = results["num_org_mem"]
                    if(grdyr == ""):
                        grdyr = 2008
                    if(nummem == ""):
                        nummem = 267
                    if sec_spec == "":
                        sec_spec = "None"
                    row = Geriatricians(first_name = results.get("frst_nm"),
                                        last_name = results.get("lst_nm"),
                                        gender = results.get("gndr"),
                                        medical_school = results.get("med_sch"),
                                        grad_year = grdyr,
                                        facility_name = results.get("facility_name"),
                                        address = results.get("adr_ln_1"),
                                        city = results.get("citytown"),
                                        zipcode = results.get("zip_code")[:5],
                                        zipcodes_id = zipcode_o.id,
                                        num_members = nummem,
                                        secondary_specialization = sec_spec)
                    zipcode_o.geriatricians.append(row)
                    db.session.add(row)   
            else:
                print(f'Error: {response.status_code} - {response.text}')

        with open("geriatricians.json", "w") as json_file:
            json.dump(geriatricians_data, json_file, indent = 4)
        db.session.commit()